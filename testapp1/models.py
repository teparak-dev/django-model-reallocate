from django.db import models

# Create your models here.


"""
Demonstration:

This model has been moved to testapp2, while migrations will continue to live inside testapp1.

Docs: https://docs.djangoproject.com/en/4.0/ref/models/options/
"""
# class ModelTestAppOne(models.Model):
#     class Meta:
#         app_label = "testapp1"
    
#     # Created Pre Reallocation.
#     name = models.CharField(max_length=20)