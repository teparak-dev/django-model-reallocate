from django.db import models

# Create your models here.
class ModelTestAppOne(models.Model):
    class Meta:
        app_label = "testapp1"
    
    # Created Pre Reallocation.
    name = models.CharField(max_length=20)


    # Created Post Reallocation. 
    # Migration will live within the old app.
    field_post_reallocation = models.CharField(max_length=20, default=None)


class ModelTestAppTwo(models.Model):
    

    some_field = models.CharField(max_length=20)